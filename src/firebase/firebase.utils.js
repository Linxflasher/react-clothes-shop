import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/firebase-auth";

const config = {
  apiKey: "AIzaSyC5y64l0eXEMomqWFu2YKzn80xeh2B53r0",
  authDomain: "clothes-shop-d52c2.firebaseapp.com",
  databaseURL: "https://clothes-shop-d52c2.firebaseio.com",
  projectId: "clothes-shop-d52c2",
  storageBucket: "clothes-shop-d52c2.appspot.com",
  messagingSenderId: "1043576755244",
  appId: "1:1043576755244:web:6b9c62e5c55a943db2ad89",
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;
  const userRef = firestore.doc(`users/${userAuth.uid}`);
  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();

    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData,
      });
    } catch (error) {
      console.log("Error creating user", error.message);
    }
  }
  return userRef;
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;

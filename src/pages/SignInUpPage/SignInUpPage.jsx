import React from 'react';
import "./sign-in-up-page.scss";

import SignIn from "../../components/SignIn/SignIn";
import SignUp from "../../components/SignUp/SignUp";

const SignInUpPage = () => {
    return (
        <div className="sign-in-up-page">
            <SignIn />
            <SignUp />
        </div>
    );
}

export default SignInUpPage;